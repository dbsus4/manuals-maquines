# summary

[Índex general](README.md)

# Manuals eines digitals (CAD)
* [freecad](modelling/cad/freecad_home.md) 
* [blender](modelling/blender/home.md) 

# Manuals de Tecnologies de Control Numèric (CNC)
* [làser](maquines/laser/home.md) 
  * [manteniment](maquines/laser/manteniment.md)
  * [repositori](maquines/laser/repositori.md)
* [knk](maquines/knk/home.md)
* [impressora 3d ](maquines/ffm/home.md)
* [fresadora cnc](maquines/fressadora/home.md)

## Manuals eines computacionals (programació)
* [processing](programacio/processing/home.md) audivosuals generatius/interactius
* [grasshopper](programacio/grasshopper/home.md) modelat 2d/3d computacional
* [pure data](programacio/puredata/home.md) 

## Manuals electrònica
* [electrònica](electronica/home.md) 
* [arduino](electronica/arduino/home.md) 

## Control de versions (git)
* [git](programacio/git.md) 
