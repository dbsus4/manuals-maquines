### Taller FED, Escola Massana, Barcelona
# Wiki del Laboratori d'Experimentació i Prototipat Computacional

---

## Índex


### Manuals eines digitals (CAD)
* [freecad](modelling/cad/freecad_home.md) 
* [blender](modelling/blender/home.md) 

### Manuals de Tecnologies de Control Numèric (CNC)
* [làser](maquines/laser/home.md)
* [knk](maquines/knk/home.md)
* [impressora 3d ffm](maquines/ffm/home.md)
* [fresadora cnc](maquines/fressadora/home.md)

### Manuals eines computacionals (programació)
* [processing](programacio/processing/home.md) audivosuals generatius/interactius
* [grasshopper](programacio/grasshopper/home.md) modelat 2d/3d computacional
* [pure data](programacio/puredata/home.md) 

### Manuals electrònica
* [electrònica](electronica/home.md) 
* [arduino](electronica/arduino/home.md) 

### Control de versions (git)
* [git](programacio/git.md) 

---

ONLINE: https://escola-massana.gitlab.io/fed/manuals-maquines 

---

WIKI ANTERIOR : https://github.com/escolamassana/manuals/wiki

---

