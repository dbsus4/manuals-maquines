### Láser


![laser guapa](https://gitlab.com/escola-massana/fed/manuals-maquines/raw/master/maquines/laser/img/neteja_laser_0.jpeg "Laser to guapa")

para sacar la ruta: abrir la imagen en una pestaña desde el mismo web editor, right click, ver imagen, copiar url
la url es del tipo https://gitlab.com/escola-massana/fed/manuals-maquines/raw/...

## seguretat
* filtre
* extracció

La màquina de tall làser no permet el treball amb la tapa oberta per raons de seguretat i aquest mecanisme en cap cas s'ha de modificar.


## Preparació cad
Els arxius vectorials per tallar amb màquina laser s'han d'exportar en .dwg o .dxf
El treballa amb imatge raster per generar trames i gravar-les és també una opció.
L'arxiu es pot separar en capes per facilitar el treball en el software de tall.

## Engegat màquina
Per arrencar la màquina s'han d'encendre els següents sistemes:
* extracció general: interruptor marcat amb gomet groc,  dins el quadre electrìc al costat de la porta del taller.
* refrigerant: regleta darrere de la màquina làser
* diferencial de la part frontal de la màquina
* polsador de seguretat de la part superior de la màquina (posició elevada)
* i has de asegurar-te que el control de potència del làser no es troba a 0% (làser apagat)

# Colocacio material
* posició
* enfoc - distancia boquilla

## Programa
entra en el ordenador como professor
el programa se llama SmartCarve


### neteja
usa papel de cocina y el KH 7 y pásale una limpiadita



## parametros de configuracion por materiales

proyectos seleccionados, experiemntacion, ect

