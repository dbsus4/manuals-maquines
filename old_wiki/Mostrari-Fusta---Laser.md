## Fusta DM

### 4mm
| *   | PotÃ¨ncia Max    |  PotÃ¨ncia Min | Velocitat   |
|:-------------:|:-------------:|:-------------:|-----------:|
| gravat superf |  10          | ?  |   100        |
| gravat        |  20          | ?  |   150        |
| tall Ã²ptim    |  80          |  ?  |   40         |

![alt text](https://github.com/escolamassana/manuals/blob/master/wiki/mostrari%20laser/mostrari_laser-dm_4mm.jpg  "mostres dm 4mm")

<img src="https://github.com/escolamassana/manuals/blob/master/wiki/mostrari%20laser/mostrari_laser-dm_4mm.jpg" width="200">

![alt text](https://github.com/escolamassana/manuals/blob/master/wiki/mostrari%20laser/mostrari_laser-dm_4mm_2.jpg  "mostres dm 4mm")






## Fullola

### 3mm
| *   | PotÃ¨ncia      | Velocitat   |
|:-------------:|:-------------:| -----------:|
| gravat superf |  10          |   200        |
| gravat        |  20          |   150        |
| quasi tall    |  30          |   100        |
| tall Ã²ptim    |  40          |   80         |
| tall          |  50          |   80         |
| tall          |  60          |   40         |

![alt text](https://github.com/escolamassana/manuals/blob/master/wiki/mostrari%20laser/mostrari_laser-fullola_3mm.JPG
"mostres fullola 3mm")

## FUSTA DM 5mm


|*               | gravat superf  |  gravat    | quasi tall | quasi tall |    tall    |   tall Ã²ptim    |
|----------------|----------------|------------|------------|------------|------------|-----------------|
|PotÃ¨ncia        |          10    |    20      |     30     |     40     |     50     |        65       |
|Velocitat       |          200   |    150     |     100    |     80     |     60     |        40       |

![alt text](https://github.com/escolamassana/manuals/blob/master/wiki/mostrari%20laser/G-PAR%C3%81METROS%20-%20MD%202.5%20mm..jpg?raw=true)

![alt text](https://github.com/escolamassana/manuals/blob/master/wiki/mostrari%20laser/G-PAR%C3%81METROS%20-%20MD%204%20mm..jpg?raw=true)