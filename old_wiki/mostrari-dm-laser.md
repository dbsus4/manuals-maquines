## dm

### 4mm
| *   | PotÃ¨ncia      | Velocitat   |
|:-------------:|:-------------:| -----------:|
| gravat superf |  10          |   200        |
| gravat        |  20          |   150        |
| quasi tall    |  30          |   100        |
| quasi tall    |  40          |   80         |
| tall          |  50          |   80         |
| tall Ã²ptim    |  60          |   40         |

![alt text](https://github.com/escolamassana/manuals/blob/master/wiki/mostrari%20laser/mostrari_laser-dm_4mm.jpg  "mostres corcho 4mm") 

<img src="https://github.com/escolamassana/manuals/blob/master/wiki/mostrari%20laser/mostrari_laser-dm_4mm.jpg" width="200">
