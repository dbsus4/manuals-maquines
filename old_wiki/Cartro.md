## Cartro
### 1mm

|     | PotÃ¨ncia mÃ¡x   | PotÃ¨ncia min | Velocitat   |
|:-------------:|:-------------:| :-------------:|-----------:|
| Gravat superf  |  15          |  05          |   200        |



<img src="https://github.com/escolamassana/manuals/blob/master/wiki/mostrari%20laser/mostrari_laser-cartro_1mm_2.jpg" width="460">

|     | PotÃ¨ncia mÃ¡x   | PotÃ¨ncia min | Velocitat   |
|:-------------:|:-------------:| :-------------:|-----------:|
| Quasi tall     |  20          |  10          |   200        |
<img src="https://github.com/escolamassana/manuals/blob/master/wiki/mostrari%20laser/mostrari_laser-cartro_1mm.jpg" width="460">