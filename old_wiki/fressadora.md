# CNC

## InstrucciÃ³ns d'Ãºs de la fresadora per control numÃ¨ric

### PreparaciÃ³ de la mÃ quina

1. Arrencar la fressadora (power on)
- girar el mando rodÃ³ vermell (imatge) fins que es desbloquegi i s'alci una mica

![powerON](https://github.com/escolamassana/manuals/blob/master/wiki/1_encendre.jpg)

2. Un cop encesa fer el Homing la mÃ quina (0,0,0) 
- A. la mÃ quina demana si es vol fer el homing, aceptar 

![homing](https://github.com/escolamassana/manuals/blob/master/wiki/3_homing.jpg)

- B. fer el homing manual 

![manualHoming](https://github.com/escolamassana/manuals/blob/master/wiki/3_homing2.jpg)

3. Arrencar la refrigeraciÃ³ (spindle)
- Interruptor negre al mecanisme de refrigeraciÃ³ de darrera la fresadora

![chillerON](https://github.com/escolamassana/manuals/blob/master/wiki/2_refrigeraciÃ³.jpg)

4. Espai de treball, retirar el pont i fer espai per fixar el material
- Usar les fletxes verticals i horitzontals per moure el pont

![moviments](https://github.com/escolamassana/manuals/blob/master/wiki/moviments.jpg)

5. Fixar la fressa escollida
- A. en funciÃ³ de la fressa cambiar el "collet" (ER20)

![collet](https://github.com/escolamassana/manuals/blob/master/wiki/collet.jpg)

- B. colocar la fressa al "collet" amb compte de que no caigui (es pot despuntar)

![fix1](https://github.com/escolamassana/manuals/blob/master/wiki/fixaciÃ³_2.jpg)

- C. apretar amb les claus fixes (27 / 30) lleugerament, no apretar molt fort

![fix2](https://github.com/escolamassana/manuals/blob/master/wiki/fixaciÃ³_1.jpg)

### Preparar el material a fressar

6. Fixar el material a la bancada 
- A. amb les mordasses de guÃ­a ajustant bÃ© les altures

![mordasses](https://github.com/escolamassana/manuals/blob/master/wiki/mordassa.jpg)

- B. amb un mÃ¡rtir, recomanat si s'ha de tallar peces (amb cargols o altres mecanismes de fixaciÃ³ que no interfereixin amb els moviments del pont)

### Definir origen mÃ quina-material (x0, y0, z0)

7. Definir origen

- A. posicionar X i Y amb el mando de control, fent us de les fletxes, al origen de la nostre peÃ§a

![origen](https://github.com/escolamassana/manuals/blob/master/wiki/origen.jpg)

- B. fixar l'orÃ­gen de X i de Y al mando de control amb el botÃ³ XY->0

![fixat](https://github.com/escolamassana/manuals/blob/master/wiki/origenFixat.jpg)

- C. per posicionar l'origen de la Z (altura) posicionar el sensor sobre el nostre material.

![sensor](https://github.com/escolamassana/manuals/blob/master/wiki/Z0.jpg)
![sensing](https://github.com/escolamassana/manuals/blob/master/wiki/z.jpg)

- D. pulsar el botÃ³ C.A.D. del mando de control per fixar la altura de la Z automÃ ticament

![z](https://github.com/escolamassana/manuals/blob/master/wiki/zzz.jpg)

### Mecanitzar arxiu

- A. insertar USB amb l'[arxiu](https://github.com/escolamassana/manuals/wiki/Arxiu) per mecanitzar

![usb](https://github.com/escolamassana/manuals/blob/master/wiki/meca_1.jpg)

- B. accedir a la memÃ²ria pulsant F4

![open](https://github.com/escolamassana/manuals/blob/master/wiki/meca_2.jpg)

- C. escollir memÃ²ria interna o USB

![memo](https://github.com/escolamassana/manuals/blob/master/wiki/meca_3.jpg)

- D. escollir l'arxiu a mencanitzar (pulsar F3 per anar canviant d'arxiu) i pulsar Ok (botÃ³ verd)

![file](https://github.com/escolamassana/manuals/blob/master/wiki/meca_4.jpg)

- E. al visor principal s'ha de veure el nom de l'arxiu, revisar tot i pulsar RUN (botÃ³ groc)

![run](https://github.com/escolamassana/manuals/blob/master/wiki/meca_5.jpg) 



