
# GIT

Git és un protocol per fer control de versions i molt més

Git permet anar consolidant els canvis d'un projecte digital mantenint un històric dels diferents estats consolidats (commits) detectant i dessant l'informació només dels canvis (eficiència).
Aquest registre es pot compartir i sincronitzar entre diferents màquines: per exemple entre un servidor (online) i un ordinador (local).

Actualment existeixen dos grans plataformes que desplegues gratis per projectes opensource servidor i infraestructura git:
 * Github (microsoft)
 * Gitlab (plataforma que allotja aquesta wiki :D )

## Glosari bàsic:
 * Repositori (repo): "contenidor" d'un projecte, que agrupa diferents arxius.
 * Commit:  consolidació localitzada en els temps de l'estat dels arxius. El commit resgistra els canvis des del darrer commit.
 * Pull: sincronització els canvis des del repositori local cap al repositori online.
 * Push: sincronització del canvis des del repositori online cap al repositori local.
 * Origin: instancia on-line del repositori.
 * Branch: conjunt de commits independents sota un nom donat que serveix per fer versions del projecte de forma no destructiva.
 * Master: branca principal.

##  ÚS

L'úús de git normalment éés fa a través del terminal de sistema. La terminal s'accedeix de forma diferent en funcióó del sistema operatiu.
A continuacióó s'especifiquen els comandaments a insertar en la terminal en __negreta__.

Per fer servir git és ultramega recomanable fer servir el programa des de la terminal.
Primerament hauem de baixar-nos git per al nostre ssistema operatiu.

>> Linux(Debian/Ubuntu): __sudo apt get install git__ 

**Inicialització d'un projecte**:
 * 1. Creo repo a Gitlab
 * 2. 
  * Navego amb la terminal fins a la carpeta on vull mantenir la mava instancia del repo (pwd, cd, ls, ...) (veure nota més endavant)
  * Clono el repo al meu ordinador: __git clone https://gitlab.com/ nom /projecte .git__

 * 3. Copio (copiar i enganxar, p.e.) els continguts de un projecte existent a la carpeta (veure notes més endavant) on he clonat el repo o creo els arxius del nou projecte directament en aquesta carpeta
 * 4. Afegeixo els arxius nous a git: __git add .__

**Desenvolupament __iterat__**:
 * 4. Si s'han afegit arxius nous: fer __git add .__ + __git commit -m "nota d'arxius afegits"__
 * 5. Si només he fet canvis al arxius existents: fer __git commit -a -m "nota de canvis"__

**Per sincronitzar els arxius amb el repo online**
 * 6. Actualitzar l'origen: fer __git push origin master__


### Notes

 1. Com sé en quin directori estic a la terminal: **pwd** (print working directory: retorna en quina carpeta/directori estic treballant)
 2. Per canviar de carpeta: cd  (change directory)
  * ... per pujar en el arbre de carpetes (la carpeta que conté la carpeta on estic (treballant)): **cd ..**
  * ... per ficar-me a una carpeta que es troba a la carpeta on estic: **cd nomedelacarpeta**
  * Per llistar el contingut (arxius) d'una carpeta: __ls__. 

